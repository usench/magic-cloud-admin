<p align="center">
    
</p>
<p align="center">
    <a target="_blank" href="https://www.oracle.com/technetwork/java/javase/downloads/index.html"><img src="https://img.shields.io/badge/JDK-1.8+-green.svg" /></a>
    <a href="https://search.maven.org/search?q=g:org.ssssssss%20AND%20a:magic-api">
        <img alt="maven" src="https://img.shields.io/maven-central/v/org.ssssssss/magic-api.svg?style=flat-square">
    </a>
    <a target="_blank" href="https://www.ssssssss.org"><img src="https://img.shields.io/badge/Docs-latest-blue.svg"/></a>
    <a target="_blank" href="https://github.com/ssssssss-team/magic-api/releases"><img src="https://img.shields.io/github/v/release/ssssssss-team/magic-api?logo=github"></a>
    <a target="_blank" href='https://gitee.com/ssssssss-team/magic-api'><img src="https://gitee.com/ssssssss-team/magic-api/badge/star.svg?theme=white" /></a>
    <a target="_blank" href='https://github.com/ssssssss-team/magic-api'><img src="https://img.shields.io/github/stars/ssssssss-team/magic-api.svg?style=social"/></a>
    <a target="_blank" href="LICENSE"><img src="https://img.shields.io/:license-MIT-blue.svg"></a>
    <a target="_blank" href="https://shang.qq.com/wpa/qunwpa?idkey=10faa4cf9743e0aa379a72f2ad12a9e576c81462742143c8f3391b52e8c3ed8d"><img src="https://img.shields.io/badge/QQ群-720832964-blue"></a>
</p>

[特性](#特性) | [快速开始](#快速开始) | [文档/演示](#文档演示) | [示例项目](#示例项目) | <a target="_blank" href="http://ssssssss.org/changelog.html">更新日志</a> | [项目截图](#项目截图) | [其它开源](#其它开源项目)

简介
-----------------------------------
magic-cloud-admin 是一款基于magic-api的低代码智能开发平台。引领新的开发模式，可以帮助解决Java项目90%的重复工作，让开发更多关注业务逻辑。

适用项目
-----------------------------------
magic-cloud-admin 是一个基于magic-api的快速开发平台，无需定义Controller、Service、Dao、Mapper、XML、VO等Java对象即可完成常见的CRUD功能的开发
magic-cloud-admin 低代码开发平台，可以应用在任何J2EE项目的开发中，尤其适合企业信息管理系统（MIS）、内部办公系统（OA）、企业资源计划系统（ERP）、客户关系管理系统（CRM）等，其半智能手工Merge的开发方式，可以显著提高开发效率90%以上，极大降低开发成本；
magic-cloud-admin 尤为显著的支持SAAS企业级应用开发，插件机制更好的支持了SAAS云应用需求。

为什么选择magic-cloud-admin?
-----------------------------------
* 1.采用主流框架，容易上手; 代码生成器依赖性低,很方便的扩展能力，可完全实现二次开发;
* 2.开发效率很高,采用代码生成器，单表数据模型和一对多(父子表)数据模型，增删改查功能自动生成，菜单配置直接使用；
* 3.页面校验自动生成(必须输入、数字校验、金额校验、时间空间等);
* 4.封装完善的用户基础权限、强大的数据权限、和数据字典等基础功能，直接使用无需修改
* 5.常用共通封装，各种工具类(定时任务,短信接口,邮件发送,Excel导出等),基本满足80%项目需求
* 6.集成简易报表工具，图像报表和数据导出非常方便，可极其方便的生成pdf、excel、word等报表；
* 7.查询过滤器：查询功能自动生成，后台动态拼SQL追加查询条件；支持多种匹配方式（全匹配/模糊查询/包含查询/不匹配查询）；
* 8.多数据源：及其简易的使用方式，在线配置数据源配置，便捷的从其他数据抓取数据；
* 9.国际化：支持多语言，开发国际化项目非常方便；
* 10.数据权限（精细化数据权限控制，控制到行级，列表级，表单字段级，实现不同人看不同数据，不同人对同一个页面操作不同字段
* 11.在线配置报表（无需编码，通过在线配置方式，实现曲线图，柱状图，数据等报表）
* 12.简易Excel导入导出，支持单表导出和一对多表模式导出，生成的代码自带导入导出功能
* 13.自定义表单，支持用户自定义表单布局，支持单表，一对多表单、支持select、radio、checkbox、textarea、date、popup、列表、宏等控件
* 14.专业接口对接机制，统一采用restful接口方式，集成swagger-ui在线接口文档，Jwt token安全验证，方便客户端对接
* 15.接口安全机制，可细化控制接口授权，非常简便实现不同客户端只看自己数据等控制
* 16.高级组合查询功能，在线配置支持主子表关联查询，可保存查询历史
* 17.支持二级管理员，权限细化管理
* 18.代码生成器支持resutful接口生成
* 19.支持热部署
* 20.提供简单易用的打印插件，支持谷歌、火狐、IE11+ 等各种浏览器
* 21.示例代码丰富，提供很多学习案例参考
* 22.采用maven分模块开发方式
* 23.支持菜单动态路由
* 24.权限控制采用 RBAC（Role-Based Access Control，基于角色的访问控制）

# 特性
- 支持MySQL、MariaDB、Oracle、DB2、PostgreSQL、SQLServer 等多支持jdbc规范的数据库
- 支持非关系型数据库Redis、Mongodb
- 支持分页查询以及自定义分页查询
- 支持多数据源配置，支持运行时动态添加数据源
- 支持SQL缓存，以及自定义SQL缓存
- 支持自定义JSON结果、自定义分页结果
- 支持对接口权限配置、拦截器等功能
- 支持运行时动态修改数据源
- 支持Swagger接口文档生成
- 基于[magic-script](https://gitee.com/ssssssss-team/magic-script)脚本引擎，动态编译，无需重启，实时发布
- 支持Linq式查询，关联、转换更简单
- 支持数据库事务、SQL支持拼接，占位符，判断等语法
- 支持文件上传、下载、输出图片
- 支持脚本历史版本对比与恢复
- 支持脚本代码自动提示、错误提示
- 支持导入Spring中的Bean、Java中的类
- 支持在线调试脚本引擎
- 支持自定义工具类、自定义模块包、自定义类型扩展、自定义函数等

技术架构：
-----------------------------------
#### 开发环境

- 语言：Java 8

- IDE(JAVA)： IDEA 

- IDE(前端)： WebStorm 或者 IDEA

- 依赖管理：Maven

- 数据库：MySQL5.7+  &  Oracle 11g & Sqlserver2017

- 缓存：Redis


#### 后端
- 基础框架：Spring Boot 2.3.5.RELEASE

- 微服务框架： Spring Cloud Alibaba 2.2.3.RELEASE

- 持久层框架：Mybatis-plus 3.4.1

- 安全框架：Apache Shiro 1.7.0，Jwt 3.11.0

- 微服务技术栈：Spring Cloud Alibaba、Nacos、Gateway、Sentinel、Skywalking

- 数据库连接池：阿里巴巴Druid 1.2.3

- 缓存框架：redis

- 日志打印：logback

- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。


#### 前端
 
- [Vue 2.6.10](https://cn.vuejs.org/),[Vuex](https://vuex.vuejs.org/zh/),[Vue Router](https://router.vuejs.org/zh/)
- [Axios](https://github.com/axios/axios)
- [ant-design-vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn/)
- [webpack](https://www.webpackjs.com/),[yarn](https://yarnpkg.com/zh-Hans/)
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现
- eslint，[@vue/cli 3.2.1](https://cli.vuejs.org/zh/guide)
- vue-print-nb - 打印


## 在线编辑
访问`http://localhost:9999/magic/web`进行操作

# 文档/演示

- 文档地址： 
- 在线演示： 



# 项目截图

## 整体截图
![整体截图] 
## 切换主题
![切换皮肤] 
## 代码提示
![代码提示] 
## DEBUG
![DEBUG] 
## 历史记录
![历史记录] 

# 其它开源项目
- [magic-api](https://gitee.com/ssssssss-team/magic-api)
- [magic-script，基于Java实现的脚本引擎](https://gitee.com/ssssssss-team/magic-script)
- [magic-editor，本项目的前端UI](https://gitee.com/ssssssss-team/magic-editor)
- [spider-flow，新一代爬虫平台，以图形化方式定义爬虫流程，不写代码即可完成爬虫](https://gitee.com/ssssssss-team/spider-flow)